<?php

/**
 * @file
 * Provides the Captcha The Flood administration settings.
 */

/**
 * Form callback; administrative settings for Captcha The Flood.
 */
function ctf_admin_settings($form, &$form_state) {
  $key = 'ctf_title';
  $form[$key] = array(
    '#type' => 'textfield',
    '#title' => t('Challenge page title'),
    '#default_value' => variable_get($key, 'Please confirm to continue'),
    '#required' => TRUE,
  );

  if (variable_get('captcha_add_captcha_description', TRUE)) {
    module_load_include('inc', 'captcha');
    // Use #prefix rather than #description to avoid an unnecessary fieldset,
    // including its 'CAPTCHA' legend.
    $description = _captcha_get_description();
  }
  else {
    $description = '';
  }

  $key = 'ctf_description';
  $form[$key] = array(
    '#type' => 'textarea',
    '#title' => t('Challenge description'),
    '#description' => t('Configurable description of the CAPTCHA. Leave empty to use the <a href="@url">default description used for CAPTCHA challenges</a>, which is currently !description.', array(
      '@url' => url('admin/config/people/captcha', array(
        'fragment' => 'edit-captcha-add-captcha-description',
      )),
      '!description' => $description ? '<em>"' . $description . '"</em>' : t('disabled'),
    )),
    '#default_value' => variable_get($key),
  );

  $key = 'ctf_whitelist';
  $form[$key] = array(
    '#type' => 'textarea',
    '#title' => t('Pages not to block'),
    '#description' => t("Specify pages that can be accessed even when flood control has been triggered, by using their paths. The page containing the challenge itself does not need listing here. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    '#default_value' => variable_get($key, "user\nuser/*"),
  );

  $key = 'ctf_retries';
  $form[$key] = array(
    '#type' => 'textfield',
    '#title' => t('Number of allowed attempts within timeframe before showing challenge'),
    '#default_value' => variable_get($key, 5),
    '#required' => TRUE,
    '#size' => 5,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $key = 'ctf_window';
  $form[$key] = array(
    '#type' => 'textfield',
    '#title' => t('Timeframe to monitor attempts across, in seconds'),
    '#description' => t('For example, 240 seconds for 4 minutes.'),
    '#default_value' => variable_get($key, 240),
    '#required' => TRUE,
    '#size' => 10,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'ctf_admin_settings_submit';
  return $form;
}
